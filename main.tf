provider "aws" {
    region = "us-east-1"
   
}

// creating a vpc setting its ip range
resource "aws_vpc" "my-dev-vpc" {
  cidr_block = "10.0.0.0/16" 
}
// creating a subnet in my vpc
resource "aws_subnet" "dev-subnet-1" { 
    vpc_id = aws_vpc.my-dev-vpc.id
    cidr_block = "10.0.10.0/24"
    availability_zone = "us-east-1a"
}
// to query the existing ressources on aws
